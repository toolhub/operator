package v1alpha1

const (
	// SeverityCritical describes critical severity, meaning possible down time.
	SeverityCritical = "Critical"

	// SeverityInfo describes informational condition, one that is expected and normal.
	SeverityInfo = "Info"
)

// Condition defines a generic condition.
type Condition struct {
	// Name defines the type/name of the condition. This can be used by other controllers/applications.
	Name     string `json:"name,omitempty"`
	Severity string `json:"severity,omitempty"`
	Status   string `json:"status,omitempty"`
	Started  string `json:"started,omitempty"`
}
