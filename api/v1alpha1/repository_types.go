/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// DispatcherSpec defines the desired state of the repository dispatcher.
type DispatcherSpec struct {
	// Image description here.
	Image           string `json:"image,omitempty"`
	MinimumReplicas int32  `json:"minReplicas,omitempty"`
	MaximumReplicas int32  `json:"maxReplicas,omitempty"`
}

// AgentRef holds specification with regards to function service agent.
type AgentRef struct {
	Kind string `json:"kind,omitempty"`
	Name string `json:"name,omitempty"`
}

// APISpec defines the desired state of the repository API.
type APISpec struct {
	Image              string    `json:"image,omitempty"`
	MinimumReplicas    int32     `json:"minReplicas,omitempty"`
	MaximumReplicas    int32     `json:"maxReplicas,omitempty"`
	EnableModification bool      `json:"enableModification,omitempty"`
	Agent              *AgentRef `json:"agent,omitempty"`
}

type DashboardSpec struct {
	Image           string `json:"image,omitempty"`
	MinimumReplicas int32  `json:"minReplicas,omitempty"`
	MaximumReplicas int32  `json:"maxReplicas,omitempty"`
}

// IngressTLS is similar to ingress TLS.
type IngressTLS struct {
	Hosts      []string `json:"hosts,omitempty"`
	SecretName string   `json:"secretName,omitempty"`
}

// IngressTemplateSpec describes a template for creating ingresses.
type IngressTemplateSpec struct {
	Location         string            `json:"location,omitempty"`
	Labels           map[string]string `json:"labels,omitempty"`
	Annotations      map[string]string `json:"annotations,omitempty"`
	TLS              []IngressTLS      `json:"tls,omitempty"`
	IngressClassName *string           `json:"ingressClassName,omitempty"`
}

// SeparateIngressObjectsSpec holds templates for specific components.
type SeparateIngressObjectsSpec struct {
	API        *IngressTemplateSpec `json:"api,omitempty"`
	Dispatcher *IngressTemplateSpec `json:"dispatcher,omitempty"`
	CDS        *IngressTemplateSpec `json:"cds,omitempty"`
	Dashboard  *IngressTemplateSpec `json:"dashboard,omitempty"`
}

// IngressPathSpec holds subpaths for each component.
type IngressPathSpec struct {
	API        string `json:"api,omitempty"`
	Dispatcher string `json:"dispatcher,omitempty"`
	CDS        string `json:"cds,omitempty"`
	Dashboard  string `json:"dashboard,omitempty"`
}

// IngressSpec holds ingress details.
type IngressSpec struct {
	IngressTemplateSpec    `json:",inline"`
	SeparateIngressObjects SeparateIngressObjectsSpec `json:"separateIngressObjects,omitempty"`
	Paths                  IngressPathSpec            `json:"paths,omitempty"`
}

// AccessEndpointSpec holds spec for additional access endpoints.
type AccessEndpointSpec struct {
	Hostname   string `json:"hostname,omitempty"`   // could be * meaning default or anything.
	Dispatcher string `json:"dispatcher,omitempty"` // could be slash so as to mean relative or a complete hostname.
	CDS        string `json:"cds,omitempty"`        // could be slash so as to mean relative or a copmlete hostname.
	API        string `json:"api,omitempty"`
	Dashboard  string `json:"dashboard,omitempty"`
	Insecure   bool   `json:"insecure,omitempty"`
}

// RepositorySpec defines the desired state of Repository
type RepositorySpec struct {
	// Dispatcher has docs.
	Dispatcher                DispatcherSpec        `json:"dispatcher,omitempty"`
	API                       *APISpec              `json:"api,omitempty"`
	ContentDelivery           *ContentDeliverySpec  `json:"contentDelivery,omitempty"`
	Dashboard                 *DashboardSpec        `json:"dashboard,omitempty"`
	Selector                  *metav1.LabelSelector `json:"selector,omitempty"`
	DefaultGroup              string                `json:"defaultGroup,omitempty"`
	Ingress                   *IngressSpec          `json:"ingress,omitempty"`
	AdditionalAccessEndpoints []AccessEndpointSpec  `json:"additionalAccessEndpoints,omitempty"`
}

// FunctionRouteStatus holds status info for a function.
type FunctionRouteStatus struct {
	Function         string `json:"function,omitempty"`
	DispatcherStatus string `json:"dispatcherStatus,omitempty"`
}

// RepositoryStatus defines the observed state of Repository
type RepositoryStatus struct {
	Conditions      []Condition           `json:"conditions,omitempty"`
	ReadyReplicas   uint8                 `json:"readyReplicas,omitempty"`
	DesiredReplicas uint8                 `json:"desiredReplicas,omitempty"`
	Rotues          []FunctionRouteStatus `json:"rotues,omitempty"`
}

// ContentDeliverySpec provides spec related to the content delivery to use for the repository.
// If a custom service is used, the service must respond to content delivery probes and must
// set appropriate CSP header for security.
type ContentDeliverySpec struct {
	ProbeImage      string `json:"probeImage,omitempty"`
	Type            string `json:"type,omitempty"`
	ServerImage     string `json:"serverImage,omitempty"`
	MinimumReplicas int32  `json:"minReplicas,omitempty"`
	MaximumReplicas int32  `json:"maxReplicas,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status

// Repository is the Schema for the repositories API
type Repository struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   RepositorySpec   `json:"spec,omitempty"`
	Status RepositoryStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// RepositoryList contains a list of Repository
type RepositoryList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Repository `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Repository{}, &RepositoryList{})
}
