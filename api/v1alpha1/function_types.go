/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	// PhaseRunning indicates the function is ready to take requests.
	PhaseRunning = "Running"

	// PhaseInitializing indicates the function is initializing and is not ready to take requests.
	PhaseInitializing = "Initializing"

	// PhaseError indicates a fatal error in starting the function.
	PhaseError = "Error"
)

// BackendSpec defines topology of the function.
type BackendSpec struct {
	Image string `json:"image,omitempty"`
	Type  string `json:"type,omitempty"`
	Port  int32  `json:"port,omitempty"`
}

type FrontendSpec struct {
	Source string `json:"source,omitempty"`
}

// FunctionSpec defines the desired state of Function
type FunctionSpec struct {
	Backend         BackendSpec                 `json:"backend,omitempty"`
	MinimumReplicas int32                       `json:"minReplicas,omitempty"`
	MaximumReplicas int32                       `json:"maxReplicas,omitempty"`
	Resources       corev1.ResourceRequirements `json:"resources,omitempty"`
	Frontend        *FrontendSpec               `json:"frontEnd,omitempty"`
}

// FunctionStatus defines the observed state of Function
type FunctionStatus struct {
	Conditions []Condition `json:"conditions,omitempty"`
	Endpoint   string      `json:"endpoint,omitempty"`
	Phase      string      `json:"phase,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status

// Function is the Schema for the functions API
type Function struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   FunctionSpec   `json:"spec,omitempty"`
	Status FunctionStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// FunctionList contains a list of Function
type FunctionList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Function `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Function{}, &FunctionList{})
}
