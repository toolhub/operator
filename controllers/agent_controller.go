/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"log"
	"time"

	apps "k8s.io/api/apps/v1"
	core "k8s.io/api/core/v1"
	rbac "k8s.io/api/rbac/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"

	v1alpha1 "gitlab.com/toolhub/operator/api/v1alpha1"
	"gitlab.com/toolhub/operator/controllers/agent"
	"gitlab.com/toolhub/operator/controllers/manager"
)

// AgentReconciler reconciles a Agent object
type AgentReconciler struct {
	client.Client
	Log    *log.Logger
	Scheme *runtime.Scheme
	*manager.ControllerManager
}

// +kubebuilder:rbac:groups=function.toolhub.io,resources=agents,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=function.toolhub.io,resources=agents/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=,resources=serviceaccounts;rolebindings;roles,verbs=get;list;watch;create;update;patch;delete

func (r *AgentReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	item := &v1alpha1.Agent{}
	if err := r.Client.Get(ctx, req.NamespacedName, item); err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	repos := &v1alpha1.RepositoryList{}
	if err := r.Client.List(ctx, repos); err != nil {
		return ctrl.Result{RequeueAfter: 5 * time.Second}, err
	}

	selectedRepos := []v1alpha1.Repository{}
	for _, repo := range repos.Items {
		if repo.Spec.API == nil || repo.Spec.API.Agent == nil {
			continue
		}
		if repo.Spec.API.Agent.Kind == "Agent" && repo.Spec.API.Agent.Name == item.Name {
			selectedRepos = append(selectedRepos, repo)
		}
	}

	configMap, err := agent.ConfigMap(item, selectedRepos)
	if err != nil {
		return ctrl.Result{RequeueAfter: 5 * time.Second}, err
	}

	objects := manager.ObjectManagerList{
		agent.ServiceAccount(item),
		agent.Role(item),
		agent.RoleBinding(item),
		configMap,
		agent.ReplicaSet(item),
		agent.Service(item),
	}

	if err := r.InstallObjects(ctx, objects, item); err != nil {
		return ctrl.Result{RequeueAfter: 5 * time.Second}, err
	}

	return ctrl.Result{}, nil
}

func (r *AgentReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&v1alpha1.Agent{}).
		Owns(&apps.ReplicaSet{}).
		Owns(&core.ConfigMap{}).
		Owns(&core.Service{}).
		Owns(&core.ServiceAccount{}).
		Owns(&rbac.Role{}).
		Owns(&rbac.RoleBinding{}).
		Watches(
			&source.Kind{Type: &v1alpha1.Repository{}},
			handler.EnqueueRequestsFromMapFunc(handler.MapFunc(func(f client.Object) []ctrl.Request {
				// inform all repositories in the same namespace.
				requests := []ctrl.Request{}

				list := &v1alpha1.AgentList{}
				if err := r.Client.List(context.Background(), list); err != nil {
					return requests
				}

				ns := f.GetNamespace()

				for _, agent := range list.Items {
					if agent.Namespace == ns {
						requests = append(requests, ctrl.Request{
							NamespacedName: types.NamespacedName{Name: agent.Name, Namespace: agent.Namespace}})
					}
				}

				return requests
			}))).
		Complete(r)
}
