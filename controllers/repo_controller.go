/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"log"

	apps "k8s.io/api/apps/v1"
	core "k8s.io/api/core/v1"
	netv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/equality"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/source"

	v1alpha1 "gitlab.com/toolhub/operator/api/v1alpha1"
	"gitlab.com/toolhub/operator/controllers/manager"
	"gitlab.com/toolhub/operator/controllers/repo"
)

// RepositoryReconciler reconciles a Repository object
type RepositoryReconciler struct {
	client.Client
	Log    *log.Logger
	Scheme *runtime.Scheme
	*manager.ControllerManager
}

func (r *RepositoryReconciler) reconcileDispatcher(ctx context.Context, item *v1alpha1.Repository) error {
	objects := manager.ObjectManagerList{
		repo.DispatcherService(item),
		repo.DispatcherDeployment(item),
	}
	return r.InstallObjects(ctx, objects, item)
}

func (r *RepositoryReconciler) reconcileAPI(ctx context.Context, item *v1alpha1.Repository) error {
	shouldExist := item.Spec.API != nil
	if !shouldExist {
		objects := []client.Object{
			&apps.Deployment{ObjectMeta: meta.ObjectMeta{Name: repo.APIDeploymentName(item), Namespace: item.Namespace}},
			&core.Service{ObjectMeta: meta.ObjectMeta{Name: repo.APIServiceGRPCName(item), Namespace: item.Namespace}},
			&core.Service{ObjectMeta: meta.ObjectMeta{Name: repo.APIServiceHTTPName(item), Namespace: item.Namespace}},
			&core.Service{ObjectMeta: meta.ObjectMeta{Name: repo.APISecretName(item), Namespace: item.Namespace}},
		}
		return r.DeleteObjects(ctx, objects)
	}

	objects := manager.ObjectManagerList{
		repo.APIDeployment(item),
		repo.APIServiceGRPC(item),
		repo.APIServiceHTTP(item),
	}

	if item.Spec.API.EnableModification && item.Spec.API.Agent != nil {
		objects = append(objects, repo.APISecret(item))
	} else {
		err := r.DeleteObjects(ctx, []client.Object{
			&core.Secret{ObjectMeta: meta.ObjectMeta{Name: repo.APISecretName(item), Namespace: item.Namespace}}})

		if err != nil && !apierrors.IsNotFound(err) {
			return err
		}
	}

	return r.InstallObjects(ctx, objects, item)
}

func (r *RepositoryReconciler) reconcileCDS(ctx context.Context, item *v1alpha1.Repository) error {
	shouldExist := item.Spec.ContentDelivery != nil
	if !shouldExist {
		objects := []client.Object{
			&apps.Deployment{ObjectMeta: meta.ObjectMeta{Name: repo.CDSDeploymentName(item), Namespace: item.Namespace}},
			&core.Service{ObjectMeta: meta.ObjectMeta{Name: repo.CDSServiceName(item), Namespace: item.Namespace}},
		}
		return r.DeleteObjects(ctx, objects)
	}

	objects := manager.ObjectManagerList{
		repo.CDSDeployment(item),
		repo.CDSService(item),
	}

	return r.InstallObjects(ctx, objects, item)
}

func (r *RepositoryReconciler) reconcileDashboard(ctx context.Context, item *v1alpha1.Repository) error {
	shouldExist := item.Spec.Dashboard != nil
	if !shouldExist {
		objects := []client.Object{
			&apps.Deployment{ObjectMeta: meta.ObjectMeta{Name: repo.DashboardDeploymentName(item), Namespace: item.Namespace}},
			&core.Service{ObjectMeta: meta.ObjectMeta{Name: repo.DashboardServiceName(item), Namespace: item.Namespace}},
		}
		return r.DeleteObjects(ctx, objects)
	}

	objects := manager.ObjectManagerList{
		repo.DashboardDeployment(item),
		repo.DashboardService(item),
	}

	return r.InstallObjects(ctx, objects, item)
}

func (r *RepositoryReconciler) reconcileConfig(ctx context.Context, item *v1alpha1.Repository, funcs []v1alpha1.Function) error {
	configMapManager, err := repo.RepositoryConfig(item, funcs)
	if err != nil {
		return err
	}

	return r.InstallObjects(ctx, manager.ObjectManagerList{configMapManager}, item)
}

func (r *RepositoryReconciler) ensureStatus(ctx context.Context, repo *v1alpha1.Repository, funcs []v1alpha1.Function) error {
	oldStatus := repo.DeepCopy()
	repo.Status.Rotues = []v1alpha1.FunctionRouteStatus{}
	for _, item := range funcs {
		repo.Status.Rotues = append(repo.Status.Rotues, v1alpha1.FunctionRouteStatus{
			Function:         fmt.Sprintf("%s/%s", repo.Spec.DefaultGroup, item.Name),
			DispatcherStatus: item.Status.Phase,
		})
	}
	if err := r.Status().Patch(ctx, repo, client.MergeFrom(oldStatus)); err != nil {
		return err
	}
	return nil
}

func (r *RepositoryReconciler) getSelectedFunctions(ctx context.Context, repo *v1alpha1.Repository) ([]v1alpha1.Function, error) {
	functions := &v1alpha1.FunctionList{}
	if err := r.Client.List(ctx, functions); err != nil {
		return nil, fmt.Errorf("failed to get functions: %s", err)
	}

	items := []v1alpha1.Function{}

	for _, item := range functions.Items {
		if exclusiveRepo, ok := item.Annotations[manager.AnnotationRepo]; ok {
			if exclusiveRepo == repo.Name {
				items = append(items, item)
			}
			continue
		}
		if repo.Spec.Selector == nil || equality.Semantic.DeepDerivative(repo.Spec.Selector.MatchLabels, item.ObjectMeta.Labels) {
			items = append(items, item)
		}
	}

	return items, nil
}

// +kubebuilder:rbac:groups=function.toolhub.io,resources=repositories,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=function.toolhub.io,resources=repositories/status,verbs=get;update;patch

func (r *RepositoryReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	repo := v1alpha1.Repository{}
	err := r.Client.Get(ctx, req.NamespacedName, &repo)
	if err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	selectedFunctions, err := r.getSelectedFunctions(ctx, &repo)
	if err != nil {
		return ctrl.Result{}, fmt.Errorf("could not select functions: %s", err)
	}

	// Create/Update function config.
	if err := r.reconcileConfig(ctx, &repo, selectedFunctions); err != nil {
		return ctrl.Result{}, fmt.Errorf("error updating function config for %s: %s", repo.Name, err)
	}

	// Reconcile Dispatcher
	if err := r.reconcileDispatcher(ctx, &repo); err != nil {
		return ctrl.Result{}, fmt.Errorf("error updating dispatcher: %s", err)
	}

	// Reconcile API
	if err := r.reconcileAPI(ctx, &repo); err != nil {
		return ctrl.Result{}, fmt.Errorf("error updating API: %s", err)
	}

	// Reconcile CDS
	if err := r.reconcileCDS(ctx, &repo); err != nil {
		return ctrl.Result{}, fmt.Errorf("error updating CDS: %s", err)
	}

	// Reconcile Dashboard
	if err := r.reconcileDashboard(ctx, &repo); err != nil {
		return ctrl.Result{}, fmt.Errorf("error updating Dashboard: %s", err)
	}

	// Create Ingress Objects if needed.
	if err := r.updateOrCreateIngress(ctx, &repo); err != nil {
		return ctrl.Result{}, fmt.Errorf("error setting up ingress: %s", err)
	}

	if err := r.ensureStatus(ctx, &repo, selectedFunctions); err != nil {
		return ctrl.Result{}, fmt.Errorf("error updating status for %s: %s", repo.Name, err)
	}

	return ctrl.Result{}, nil
}

func (r *RepositoryReconciler) SetupWithManager(mgr ctrl.Manager) error {
	ignoreMetaDataChanges := predicate.Funcs{
		UpdateFunc: func(e event.UpdateEvent) bool {
			switch obj := e.ObjectOld.(type) {
			case *apps.Deployment:
				newObj := e.ObjectNew.(*apps.Deployment)
				result := equality.Semantic.DeepEqual(obj.Spec, newObj.Spec) &&
					equality.Semantic.DeepEqual(obj.Status, newObj.Status)
				return !result
			case *core.ConfigMap:
				newObj := e.ObjectNew.(*core.ConfigMap)
				result := equality.Semantic.DeepEqual(obj.BinaryData, newObj.BinaryData) &&
					equality.Semantic.DeepEqual(obj.Data, newObj.Data)
				return !result
			}
			return true
		},
	}

	return ctrl.NewControllerManagedBy(mgr).
		For(&v1alpha1.Repository{}).
		WithEventFilter(ignoreMetaDataChanges).
		Owns(&apps.Deployment{}).
		Owns(&core.ConfigMap{}).
		Owns(&core.Service{}).
		Owns(&netv1.Ingress{}).
		Watches(
			&source.Kind{Type: &v1alpha1.Function{}},
			handler.EnqueueRequestsFromMapFunc(handler.MapFunc(func(f client.Object) []ctrl.Request {
				// inform all repositories in the same namespace.
				requests := []ctrl.Request{}

				list := &v1alpha1.RepositoryList{}
				if err := r.Client.List(context.Background(), list); err != nil {
					return requests
				}

				ns := f.GetNamespace()

				for _, repo := range list.Items {
					if repo.Namespace == ns {
						requests = append(requests, ctrl.Request{
							NamespacedName: types.NamespacedName{Name: repo.Name, Namespace: repo.Namespace}})
					}
				}

				return requests
			}))).
		Complete(r)
}
