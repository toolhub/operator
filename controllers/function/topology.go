package function

import (
	"gitlab.com/toolhub/operator/api/v1alpha1"
	core "k8s.io/api/core/v1"
)

// TopologyHandler describes the ability to create pods and services for functions.
type TopologyHandler interface {
	CreatePodTemplate(*v1alpha1.Function) (*core.PodTemplateSpec, error)
	CreateService(*v1alpha1.Function) (*core.Service, error)
}
