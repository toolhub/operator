package function

import (
	"fmt"

	"gitlab.com/toolhub/operator/api/v1alpha1"
	"gitlab.com/toolhub/operator/controllers/manager"
	apps "k8s.io/api/apps/v1"
	core "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
)

const (
	imageCFI     = "registry.gitlab.com/toolhub/runners:latest"
	portCFI  int = 5000
)

// functionResources creates default resource requirements for the function pod.
func functionResources() core.ResourceRequirements {
	return core.ResourceRequirements{
		Limits: core.ResourceList{
			core.ResourceCPU:    resource.MustParse("200m"),
			core.ResourceMemory: resource.MustParse("500Mi"),
		},
		Requests: core.ResourceList{
			core.ResourceCPU:    resource.MustParse("100m"),
			core.ResourceMemory: resource.MustParse("100Mi"),
		},
	}
}

// functionService creates a service for the related service.
func functionService(function *v1alpha1.Function, port int) *core.Service {
	labels := map[string]string{
		"toolhub.io/function": function.Name,
	}
	// TODO: setup health check.
	return &core.Service{
		ObjectMeta: meta.ObjectMeta{
			Name:      function.Name,
			Namespace: function.Namespace,
			Labels:    labels,
		},
		Spec: core.ServiceSpec{
			Ports: []core.ServicePort{
				{
					Name:       "function",
					Port:       80,
					TargetPort: intstr.FromInt(port),
				},
			},
			Selector: map[string]string{
				"toolhub.io/function": function.Name,
			},
			Type: core.ServiceTypeClusterIP,
		},
	}
}

type HTTPTopology struct{}

func (h HTTPTopology) CreatePodTemplate(function *v1alpha1.Function) (*core.PodTemplateSpec, error) {
	// TODO: set pod security context.
	template := &core.PodTemplateSpec{
		ObjectMeta: meta.ObjectMeta{
			Name:      function.Name,
			Namespace: function.Namespace,
			Labels: map[string]string{
				"toolhub.io/function":       function.Name,
				"toolhub.io/runner":         "http",
				"toolhub.io/network-policy": "limited",
			},
		},
		Spec: core.PodSpec{
			Containers: []core.Container{
				{
					Name:  "function",
					Image: function.Spec.Backend.Image,
					Ports: []core.ContainerPort{
						{
							Name:          "function",
							ContainerPort: function.Spec.Backend.Port,
						},
					},
					Resources: functionResources(),
				},
			},
			RestartPolicy:   core.RestartPolicyAlways,
			SecurityContext: &core.PodSecurityContext{},
		},
	}
	return template, nil
}

func (h HTTPTopology) CreateService(function *v1alpha1.Function) (*core.Service, error) {
	svc := functionService(function, int(function.Spec.Backend.Port))
	svc.ObjectMeta.Labels["toolhub.io/runner"] = "http"
	return svc, nil
}

type CFITopology struct{}

func (c CFITopology) CreatePodTemplate(function *v1alpha1.Function) (*core.PodTemplateSpec, error) {
	// TODO: set pod security context.
	yes := true
	var group int64 = 1000
	template := &core.PodTemplateSpec{
		ObjectMeta: meta.ObjectMeta{
			Name:      function.Name,
			Namespace: function.Namespace,
			Labels: map[string]string{
				"toolhub.io/function":       function.Name,
				"toolhub.io/runner":         "cfi",
				"toolhub.io/network-policy": "limited",
			},
		},
		Spec: core.PodSpec{
			Containers: []core.Container{
				{
					Name:            "function",
					Image:           imageCFI,
					ImagePullPolicy: core.PullAlways,
					Command: []string{
						"/opt/toolhub/cfi",
						"-w", "2",
						"--image", function.Spec.Backend.Image,
						"--address", "0.0.0.0:5000",
					},
					Ports: []core.ContainerPort{
						{
							Name:          "function",
							ContainerPort: int32(portCFI),
						},
					},
					Resources: functionResources(),
					Env: []core.EnvVar{
						{
							Name:  "DOCKER_HOST",
							Value: "unix:///run/user/1000/docker.sock",
						},
					},
					VolumeMounts: []core.VolumeMount{
						{
							Name:      "docker-unix-socket",
							MountPath: "/run/user/1000",
						},
					},
					SecurityContext: &core.SecurityContext{
						RunAsUser:  &group,
						RunAsGroup: &group,
					},
				},
				{
					Name:  "docker",
					Image: "docker:dind-rootless",
					SecurityContext: &core.SecurityContext{
						Privileged:               &yes,
						AllowPrivilegeEscalation: &yes,
					},
					Env: []core.EnvVar{
						{
							Name:  "DOCKER_TLS_CERTDIR",
							Value: "",
						},
					},
					VolumeMounts: []core.VolumeMount{
						{
							Name:      "docker-unix-socket",
							MountPath: "/run/user/1000",
						},
					},
				},
			},
			RestartPolicy:   core.RestartPolicyAlways,
			SecurityContext: &core.PodSecurityContext{},
			Volumes: []core.Volume{
				{
					Name: "docker-unix-socket",
				},
			},
		},
	}
	return template, nil
}

func (c CFITopology) CreateService(function *v1alpha1.Function) (*core.Service, error) {
	svc := functionService(function, portCFI)
	svc.ObjectMeta.Labels["toolhub.io/runner"] = "cfi"
	return svc, nil
}

func makeTopologyHandler(function *v1alpha1.Function) (TopologyHandler, error) {
	switch function.Spec.Backend.Type {
	case "cfi":
		return CFITopology{}, nil
	case "http":
		return HTTPTopology{}, nil
	default:
		return nil, fmt.Errorf("unrecognized topology type: %s", function.Spec.Backend.Type)
	}
}

func ReplicaSet(function *v1alpha1.Function) (*manager.ReplicaSetManager, error) {
	handler, err := makeTopologyHandler(function)
	if err != nil {
		return nil, err
	}

	podTemplate, err := handler.CreatePodTemplate(function)
	if err != nil {
		return nil, err
	}

	replicaSet := &apps.ReplicaSet{
		ObjectMeta: meta.ObjectMeta{
			Namespace: function.Namespace,
			Name:      function.Name,
		},
		Spec: apps.ReplicaSetSpec{
			Replicas: &function.Spec.MinimumReplicas,
			Selector: &meta.LabelSelector{
				MatchLabels: podTemplate.ObjectMeta.Labels,
			},
			Template: *podTemplate,
		},
	}

	return manager.NewReplicaSetManager(replicaSet), nil
}

func Service(function *v1alpha1.Function) (*manager.ServiceManager, error) {
	handler, err := makeTopologyHandler(function)
	if err != nil {
		return nil, err
	}

	svc, err := handler.CreateService(function)
	if err != nil {
		return nil, err
	}

	return manager.NewServiceManager(svc), nil
}
