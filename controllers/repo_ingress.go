package controllers

import (
	"context"
	"fmt"
	"net/url"
	"strings"

	functionv1alpha1 "gitlab.com/toolhub/operator/api/v1alpha1"
	"gitlab.com/toolhub/operator/controllers/manager"
	netv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/equality"
	"k8s.io/apimachinery/pkg/api/errors"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type endpointRegistry map[string][]netv1.HTTPIngressPath

func (e endpointRegistry) Rules() []netv1.IngressRule {
	rules := []netv1.IngressRule{}
	for host, paths := range e {
		rules = append(rules, netv1.IngressRule{
			Host: host,
			IngressRuleValue: netv1.IngressRuleValue{
				HTTP: &netv1.HTTPIngressRuleValue{
					Paths: paths,
				},
			},
		})
	}
	return rules
}

func cutHostname(value string) (string, string, error) {
	u, err := url.Parse("tcp://" + value)
	if err != nil {
		return "", "", err
	}
	subpath := u.Path
	if len(subpath) == 0 {
		subpath = "/"
	}
	return u.Hostname(), subpath, nil
}

func addEntry(registry endpointRegistry, base, address string, backend netv1.IngressBackend) error {
	exact := netv1.PathTypePrefix
	switch {
	case len(address) == 0:
	case strings.HasPrefix(address, "/"):
		path := netv1.HTTPIngressPath{Path: address, PathType: &exact, Backend: backend}
		registry[base] = append(registry[base], path)
	default:
		host, subpath, err := cutHostname(address)
		if err != nil {
			return err
		}
		path := netv1.HTTPIngressPath{Path: subpath, PathType: &exact, Backend: backend}
		registry[host] = append(registry[host], path)
	}
	return nil
}

func addAPIEntryRPC(registry endpointRegistry, repo *functionv1alpha1.Repository) error {
	if repo.Spec.API == nil {
		return nil
	}
	backend := netv1.IngressBackend{
		Service: &netv1.IngressServiceBackend{
			Name: fmt.Sprintf("%s-api", repo.Name),
			Port: netv1.ServiceBackendPort{Name: "grpc"},
		},
	}
	ingress := repo.Spec.Ingress.SeparateIngressObjects.API
	if ingress != nil {
		host, _, err := cutHostname(ingress.Location)
		if err != nil {
			return err
		}
		return addEntry(registry, "", host, backend)
	}
	return addEntry(registry, repo.Spec.Ingress.Location, "/", backend)
}

func addAPIEntryHTTP(registry endpointRegistry, repo *functionv1alpha1.Repository) error {
	if repo.Spec.API == nil {
		return nil
	}
	backend := netv1.IngressBackend{
		Service: &netv1.IngressServiceBackend{
			Name: fmt.Sprintf("%s-api-grpc-web", repo.Name),
			Port: netv1.ServiceBackendPort{Name: "http"},
		},
	}
	ingress := repo.Spec.Ingress.SeparateIngressObjects.API
	if ingress != nil {
		return addEntry(registry, "", ingress.Location, backend)
	}
	return addEntry(registry, repo.Spec.Ingress.Location, repo.Spec.Ingress.Paths.API, backend)
}

func addCDSEntry(registry endpointRegistry, repo *functionv1alpha1.Repository) error {
	if repo.Spec.ContentDelivery == nil {
		return nil
	}
	backend := netv1.IngressBackend{
		Service: &netv1.IngressServiceBackend{
			Name: fmt.Sprintf("%s-cds", repo.Name),
			Port: netv1.ServiceBackendPort{Name: "http"},
		},
	}
	ingress := repo.Spec.Ingress.SeparateIngressObjects.CDS
	if ingress != nil {
		return addEntry(registry, "", ingress.Location, backend)
	}
	return addEntry(registry, repo.Spec.Ingress.Location, repo.Spec.Ingress.Paths.CDS, backend)
}

func addDashboardEntry(registry endpointRegistry, repo *functionv1alpha1.Repository) error {
	if repo.Spec.Dashboard == nil {
		return nil
	}
	backend := netv1.IngressBackend{
		Service: &netv1.IngressServiceBackend{
			Name: fmt.Sprintf("%s-dashboard", repo.Name),
			Port: netv1.ServiceBackendPort{Name: "http"},
		},
	}
	ingress := repo.Spec.Ingress.SeparateIngressObjects.Dashboard
	if ingress != nil {
		return addEntry(registry, "", ingress.Location, backend)
	}
	return addEntry(registry, repo.Spec.Ingress.Location, repo.Spec.Ingress.Paths.Dashboard, backend)
}

func addDispatcherEntry(registry endpointRegistry, repo *functionv1alpha1.Repository) error {
	backend := netv1.IngressBackend{
		Service: &netv1.IngressServiceBackend{
			Name: fmt.Sprintf("%s-dispatcher", repo.Name),
			Port: netv1.ServiceBackendPort{Name: "http"},
		},
	}
	ingress := repo.Spec.Ingress.SeparateIngressObjects.Dispatcher
	if ingress != nil {
		return addEntry(registry, "", ingress.Location, backend)
	}
	return addEntry(registry, repo.Spec.Ingress.Location, repo.Spec.Ingress.Paths.Dispatcher, backend)
}

func (r *RepositoryReconciler) tryDeleteIngress(ctx context.Context, name, namespace string) error {
	ingress := &netv1.Ingress{}
	if err := r.Client.Get(ctx, client.ObjectKey{Name: name, Namespace: namespace}, ingress); err != nil {
		return client.IgnoreNotFound(err)
	}
	if err := r.Client.Delete(ctx, ingress); err != nil {
		return client.IgnoreNotFound(err)
	}
	return nil
}

func makeIngress(item *functionv1alpha1.Repository, template *functionv1alpha1.IngressTemplateSpec, registry endpointRegistry, name string) *netv1.Ingress {
	labels := map[string]string{}
	annotations := map[string]string{}
	if template.Labels != nil {
		for key, value := range template.Labels {
			labels[key] = value
		}
	}
	if template.Annotations != nil {
		for key, value := range template.Annotations {
			annotations[key] = value
		}
	}
	labels[manager.LabelToolHub] = item.Name
	labels[manager.LabelComponent] = fmt.Sprintf("ingress-%s", name)

	tls := make([]netv1.IngressTLS, 0, len(template.TLS))
	for _, item := range template.TLS {
		tls = append(tls, netv1.IngressTLS{
			Hosts:      item.Hosts,
			SecretName: item.SecretName,
		})
	}

	return &netv1.Ingress{
		ObjectMeta: metav1.ObjectMeta{
			Name:        fmt.Sprintf("%s-%s", item.Name, name),
			Namespace:   item.Namespace,
			Labels:      labels,
			Annotations: annotations,
		},
		Spec: netv1.IngressSpec{
			Rules:            registry.Rules(),
			IngressClassName: template.IngressClassName,
			TLS:              tls,
		},
	}
}

func (r *RepositoryReconciler) deleteAllIngressBut(ctx context.Context, repoName string, names []string) error {
	list := &netv1.IngressList{}
	if err := r.Client.List(ctx, list, client.MatchingLabels{manager.LabelToolHub: repoName}); err != nil {
		return err
	}
	n := map[string]struct{}{}
	for _, item := range names {
		n[item] = struct{}{}
	}
	for _, item := range list.Items {
		if _, ok := n[item.ObjectMeta.Name]; !ok {
			if err := r.Client.Delete(ctx, &item); err != nil {
				if !errors.IsNotFound(err) {
					return err
				}
			}
		}
	}
	return nil
}

func (r *RepositoryReconciler) updateOrCreateIngress(ctx context.Context, repo *functionv1alpha1.Repository) error {
	shouldExist := repo.Spec.Ingress != nil
	if !shouldExist {
		return r.deleteAllIngressBut(ctx, repo.Name, []string{})
	}

	items := []*netv1.Ingress{}

	// add/remove them
	registry := endpointRegistry{}

	// API
	apiRegistry := endpointRegistry{}
	if err := addAPIEntryRPC(apiRegistry, repo); err != nil {
		return err
	}
	if len(apiRegistry) > 0 {
		var template *functionv1alpha1.IngressTemplateSpec
		if repo.Spec.Ingress.SeparateIngressObjects.API != nil {
			template = repo.Spec.Ingress.SeparateIngressObjects.API
			gatewayRegistry := endpointRegistry{}
			if err := addAPIEntryHTTP(gatewayRegistry, repo); err != nil {
				return err
			}
			httpIngress := makeIngress(repo, template, gatewayRegistry, "api-grpc-web")
			items = append(items, httpIngress)
		} else {
			template = &repo.Spec.Ingress.IngressTemplateSpec
			if err := addAPIEntryHTTP(registry, repo); err != nil {
				return err
			}
		}
		ingress := makeIngress(repo, template, apiRegistry, "api")
		ingress.ObjectMeta.Annotations["ingress.kubernetes.io/protocol"] = "h2c"
		items = append(items, ingress)
	}

	// CDS
	if repo.Spec.Ingress.SeparateIngressObjects.CDS != nil {
		r := endpointRegistry{}
		if err := addCDSEntry(r, repo); err != nil {
			return err
		}
		if len(r) > 0 {
			items = append(items, makeIngress(repo, repo.Spec.Ingress.SeparateIngressObjects.CDS, r, "cds"))
		}
	} else {
		if err := addCDSEntry(registry, repo); err != nil {
			return err
		}
	}

	// Dispatcher
	if repo.Spec.Ingress.SeparateIngressObjects.Dispatcher != nil {
		r := endpointRegistry{}
		if err := addDispatcherEntry(r, repo); err != nil {
			return err
		}
		if len(r) > 0 {
			items = append(items, makeIngress(repo, repo.Spec.Ingress.SeparateIngressObjects.Dispatcher, r, "dispatcher"))
		}
	} else {
		if err := addDispatcherEntry(registry, repo); err != nil {
			return err
		}
	}

	// Dashboard
	if repo.Spec.Ingress.SeparateIngressObjects.Dashboard != nil {
		r := endpointRegistry{}
		if err := addDashboardEntry(r, repo); err != nil {
			return err
		}
		if len(r) > 0 {
			items = append(items, makeIngress(repo, repo.Spec.Ingress.SeparateIngressObjects.Dashboard, r, "dashboard"))
		}
	} else {
		if err := addDashboardEntry(registry, repo); err != nil {
			return err
		}
	}

	if len(registry) > 0 {
		items = append([]*netv1.Ingress{makeIngress(repo, &repo.Spec.Ingress.IngressTemplateSpec, registry, "http")}, items...)
	}

	names := []string{}

	for _, item := range items {
		names = append(names, item.Name)

		if err := ctrl.SetControllerReference(repo, item, r.Scheme); err != nil {
			return fmt.Errorf("could not set owner reference on ingress: %s", err)
		}

		existingIngress := &netv1.Ingress{}
		err := r.Client.Get(ctx, client.ObjectKey{Name: item.Name, Namespace: item.Namespace}, existingIngress)

		if apierrors.IsNotFound(err) {
			// Create the deployment.
			if err := r.Client.Create(ctx, item); err != nil {
				return fmt.Errorf("could not create ingress: %s", err)
			}
			continue
		}

		if err != nil {
			return fmt.Errorf("could not retrieve ingress: %s", err)
		}

		if equality.Semantic.Equalities.DeepEqual(item.Spec, existingIngress.Spec) &&
			equality.Semantic.Equalities.DeepDerivative(item.ObjectMeta.Annotations, existingIngress.ObjectMeta.Annotations) &&
			equality.Semantic.Equalities.DeepDerivative(item.ObjectMeta.Labels, existingIngress.ObjectMeta.Labels) {
			continue
		}

		existingIngress.Spec = item.Spec
		if len(item.Labels) > 0 {
			if existingIngress.ObjectMeta.Labels == nil {
				existingIngress.ObjectMeta.Labels = map[string]string{}
			}
			for key, value := range item.Labels {
				existingIngress.Labels[key] = value
			}
		}
		if len(item.Annotations) > 0 {
			if existingIngress.ObjectMeta.Annotations == nil {
				existingIngress.ObjectMeta.Annotations = map[string]string{}
			}
			for key, value := range item.Annotations {
				existingIngress.Annotations[key] = value
			}
		}
		if err := r.Client.Update(ctx, existingIngress); err != nil {
			return fmt.Errorf("could not patch ingress: %s", err)
		}

	}

	if err := r.deleteAllIngressBut(ctx, repo.Name, names); err != nil {
		return err
	}

	return nil
}
