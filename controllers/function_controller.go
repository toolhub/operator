/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"log"
	"time"

	v1alpha1 "gitlab.com/toolhub/operator/api/v1alpha1"
	"gitlab.com/toolhub/operator/controllers/function"
	"gitlab.com/toolhub/operator/controllers/manager"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// FunctionReconciler reconciles a Function object
type FunctionReconciler struct {
	client.Client
	Log    *log.Logger
	Scheme *runtime.Scheme
	*manager.ControllerManager
}

// +kubebuilder:rbac:groups=function.toolhub.io,resources=functions,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=function.toolhub.io,resources=functions/status,verbs=get;update;patch

func (r *FunctionReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {

	f := &v1alpha1.Function{}
	err := r.Client.Get(ctx, req.NamespacedName, f)
	if err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	replicaSet, err := function.ReplicaSet(f)
	if err != nil {
		return ctrl.Result{RequeueAfter: 5 * time.Second}, err
	}

	service, err := function.Service(f)
	if err != nil {
		return ctrl.Result{RequeueAfter: 5 * time.Second}, err
	}
	objects := manager.ObjectManagerList{replicaSet, service}

	if err := r.InstallObjects(ctx, objects, f); err != nil {
		return ctrl.Result{RequeueAfter: 5 * time.Second}, err
	}

	needsUpdate := false
	actualEndpoint := fmt.Sprintf("http://%s", service.Service().ObjectMeta.Name)
	if f.Status.Endpoint != actualEndpoint {
		f.Status.Endpoint = actualEndpoint
		needsUpdate = true
	}

	var actualPhase string
	switch {
	case replicaSet.ReplicaSet().Status.AvailableReplicas == *replicaSet.ReplicaSet().Spec.Replicas:
		actualPhase = v1alpha1.PhaseRunning
	default:
		actualPhase = v1alpha1.PhaseInitializing
	}

	if f.Status.Phase != actualPhase {
		f.Status.Phase = actualPhase
		needsUpdate = true
	}

	if needsUpdate {
		if err = r.Client.Status().Update(ctx, f); err != nil {
			return ctrl.Result{}, err
		}
	}

	return ctrl.Result{}, nil
}

func (r *FunctionReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&v1alpha1.Function{}).
		Owns(&appsv1.ReplicaSet{}).
		Owns(&corev1.Service{}).
		Complete(r)
}
