package manager

import (
	"errors"

	apps "k8s.io/api/apps/v1"
	core "k8s.io/api/core/v1"
	rbac "k8s.io/api/rbac/v1"
	"k8s.io/apimachinery/pkg/api/equality"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// MergeMap applies dest to src and returns it.
func MergeMap(src, dest map[string]string) map[string]string {
	if dest == nil {
		return src
	}
	for key, value := range src {
		dest[key] = value
	}
	return dest
}

// MapUnion returns an updated map if needed.
func MapUnion(src, dest map[string]string) (map[string]string, bool) {
	if !equality.Semantic.DeepDerivative(src, dest) {
		return MergeMap(src, dest), true
	}
	return dest, false
}

// UpdateObjectMeta s
func UpdateObjectMeta(src, dest meta.ObjectMeta) bool {
	updated := false
	if labels, updated := MapUnion(src.Labels, dest.Labels); updated {
		dest.SetLabels(labels)
		updated = true
	}
	if annotations, updated := MapUnion(src.Annotations, dest.Annotations); updated {
		dest.SetAnnotations(annotations)
		updated = true
	}
	return updated
}

type identifier struct {
	id string
}

func (i identifier) ID() string {
	return i.id
}

// ConfigMapManager is a wrapper for ConfigMaps.
type ConfigMapManager struct {
	object *core.ConfigMap
	identifier
}

// NewConfigMapManager creates and initializes a new config map manager based on a desired config map.
func NewConfigMapManager(configMap *core.ConfigMap) *ConfigMapManager {
	return &ConfigMapManager{
		object: configMap,
	}
}

// Get returns the current relavent ConfigMap.
func (c *ConfigMapManager) Get() client.Object {
	return c.object
}

// Get returns the current relavent ConfigMap.
func (c *ConfigMapManager) ConfigMap() *core.ConfigMap {
	return c.object
}

func (c *ConfigMapManager) New() client.Object {
	return &core.ConfigMap{}
}

// Update replaces the internal object and modifies the given object so it matches the desired object.
func (c *ConfigMapManager) Update(current client.Object) (UpdateType, error) {
	configMap, ok := current.(*core.ConfigMap)
	if !ok {
		return NoAction, errors.New("incompatible type passed to update ConfigMap, only ConfigMap types are allowed")
	}

	needsUpdate := false

	if !equality.Semantic.DeepEqual(c.object.BinaryData, configMap.BinaryData) {
		needsUpdate = true
		configMap.BinaryData = c.object.BinaryData
	}

	if !equality.Semantic.DeepEqual(c.object.Data, configMap.Data) {
		needsUpdate = true
		configMap.Data = c.object.Data
	}

	if UpdateObjectMeta(c.object.ObjectMeta, configMap.ObjectMeta) {
		needsUpdate = true
	}

	c.object = configMap

	if needsUpdate {
		return NeedsUpdate, nil
	}
	return NoAction, nil
}

// SecretManager is a wrapper for Secrets.
type SecretManager struct {
	object *core.Secret
	identifier
	randomFields []string
}

// NewSecretManager creates and initializes a new config map manager based on a desired config map.
func NewSecretManager(secret *core.Secret, randomFields ...string) *SecretManager {
	return &SecretManager{
		object:       secret,
		randomFields: randomFields,
	}
}

func (c *SecretManager) New() client.Object {
	return &core.Secret{}
}

// Get returns the current relavent Secret.
func (c *SecretManager) Secret() *core.Secret {
	return c.object
}

func (c *SecretManager) Get() client.Object {
	return c.object
}

// Update replaces the internal object and modifies the given object so it matches the desired object.
func (c *SecretManager) Update(current client.Object) (UpdateType, error) {
	secret, ok := current.(*core.Secret)
	if !ok {
		return NoAction, errors.New("incompatible type passed to update Secret, only Secret types are allowed")
	}

	needsUpdate := false

	if secret.Data != nil {
		for _, field := range c.randomFields {
			if value, ok := secret.Data[field]; ok && len(value) > 0 {
				if c.object.Data == nil {
					c.object.Data = map[string][]byte{}
				}
				c.object.Data[field] = value
				delete(c.object.StringData, field)
				continue
			}
		}
	}

	if !equality.Semantic.DeepEqual(c.object.Data, secret.Data) {
		needsUpdate = true
		secret.Data = c.object.Data
	}

	if !equality.Semantic.DeepEqual(c.object.StringData, secret.StringData) {
		needsUpdate = true
		secret.StringData = c.object.StringData
	}

	if UpdateObjectMeta(c.object.ObjectMeta, secret.ObjectMeta) {
		needsUpdate = true
	}

	c.object = secret

	if needsUpdate {
		return NeedsUpdate, nil
	}
	return NoAction, nil
}

// ServiceManager is a wrapper for Services.
type ServiceManager struct {
	object *core.Service
	identifier
}

// NewServiceManager creates and initializes a new config map manager based on a desired config map.
func NewServiceManager(service *core.Service) *ServiceManager {
	return &ServiceManager{
		object: service,
	}
}

func (d *ServiceManager) New() client.Object {
	return &core.Service{}
}

// Get returns the current relavent Service.
func (c *ServiceManager) Get() client.Object {
	return c.object
}

// Service returns the current relavent Service.
func (c *ServiceManager) Service() *core.Service {
	return c.object
}

// Update replaces the internal object and modifies the given object so it matches the desired object.
func (c *ServiceManager) Update(current client.Object) (UpdateType, error) {
	service, ok := current.(*core.Service)
	if !ok {
		return NoAction, errors.New("incompatible type passed to update Service, only Service types are allowed")
	}

	updateType := NoAction

	if !equality.Semantic.DeepDerivative(c.object.Spec.Ports, service.Spec.Ports) {
		updateType.Upgrade(NeedsUpdate)
		service.Spec.Ports = c.object.Spec.Ports
	}

	if !equality.Semantic.DeepEqual(c.object.Spec.Selector, service.Spec.Selector) {
		updateType.Upgrade(NeedsReplace)
		service.Spec.Selector = c.object.Spec.Selector
	}

	if UpdateObjectMeta(c.object.ObjectMeta, service.ObjectMeta) {
		updateType.Upgrade(NeedsUpdate)
	}

	c.object = service
	return updateType, nil
}

type StatefulSetManager struct {
	object *apps.StatefulSet
	identifier
}

func (s *StatefulSetManager) Get() *apps.StatefulSet {
	return s.object
}

func (s *StatefulSetManager) Update(current client.Object) (UpdateType, error) {
	set, ok := current.(*apps.StatefulSet)
	if !ok {
		return NoAction, errors.New("Incompatible type passed, only StatefulSet objects are allowed")
	}

	updateType := NoAction

	// Selector is an immutable field, we'll have to replace the statefulset.
	if equality.Semantic.DeepEqual(s.object.Spec.Selector, set.Spec.Selector) {
		updateType.Upgrade(NeedsReplace)
	}

	if UpdateObjectMeta(s.object.ObjectMeta, set.ObjectMeta) {
		updateType.Upgrade(NeedsUpdate)
	}

	return updateType, nil
}

func NewStatefulSetManager(deployment *apps.StatefulSet) *StatefulSetManager {
	return &StatefulSetManager{object: deployment}
}

type DeploymentManager struct {
	object *apps.Deployment
	identifier
}

func (d *DeploymentManager) New() client.Object {
	return &apps.Deployment{}
}

func (d *DeploymentManager) Get() client.Object {
	return d.object
}

func (d *DeploymentManager) Deployment() *apps.Deployment {
	return d.object
}

func (d *DeploymentManager) Update(current client.Object) (UpdateType, error) {
	deployment, ok := current.(*apps.Deployment)
	if !ok {
		return NoAction, errors.New("Incompatible type passed, only Deployment objects are allowed")
	}

	updateType := NoAction

	// Selector is an immutable field, we'll have to replace the deployment.
	if !equality.Semantic.DeepEqual(d.object.Spec.Selector, deployment.Spec.Selector) {
		deployment.Spec.Selector = d.object.Spec.Selector
		updateType.Upgrade(NeedsReplace)
	}

	if !equality.Semantic.DeepDerivative(d.object.Spec, deployment.Spec) {
		deployment.Spec = d.object.Spec
		updateType.Upgrade(NeedsUpdate)
	}

	if UpdateObjectMeta(d.object.ObjectMeta, deployment.ObjectMeta) {
		updateType.Upgrade(NeedsUpdate)
	}

	d.object = deployment
	return updateType, nil
}

func NewDeploymentManager(deployment *apps.Deployment) *DeploymentManager {
	return &DeploymentManager{object: deployment}
}

type ReplicaSetManager struct {
	object *apps.ReplicaSet
	identifier
}

func (d *ReplicaSetManager) New() client.Object {
	return &apps.ReplicaSet{}
}

func (d *ReplicaSetManager) Get() client.Object {
	return d.object
}

func (d *ReplicaSetManager) ReplicaSet() *apps.ReplicaSet {
	return d.object
}

func (d *ReplicaSetManager) Update(current client.Object) (UpdateType, error) {
	repliaSet, ok := current.(*apps.ReplicaSet)
	if !ok {
		return NoAction, errors.New("Incompatible type passed, only ReplicaSet objects are allowed")
	}

	updateType := NoAction

	// Selector is an immutable field, we'll have to replace the repliaSet.
	if !equality.Semantic.DeepEqual(d.object.Spec.Selector, repliaSet.Spec.Selector) {
		repliaSet.Spec.Selector = d.object.Spec.Selector
		updateType.Upgrade(NeedsReplace)
	}

	if !equality.Semantic.DeepDerivative(d.object.Spec, repliaSet.Spec) {
		repliaSet.Spec = d.object.Spec
		updateType.Upgrade(NeedsUpdate)
	}

	if UpdateObjectMeta(d.object.ObjectMeta, repliaSet.ObjectMeta) {
		updateType.Upgrade(NeedsUpdate)
	}

	d.object = repliaSet
	return updateType, nil
}

func NewReplicaSetManager(replicaset *apps.ReplicaSet) *ReplicaSetManager {
	return &ReplicaSetManager{object: replicaset}
}

type ServiceAccountManager struct {
	object *core.ServiceAccount
	identifier
}

func (d *ServiceAccountManager) New() client.Object {
	return &core.ServiceAccount{}
}

func (d *ServiceAccountManager) Get() client.Object {
	return d.object
}

func (d *ServiceAccountManager) ServiceAccount() *core.ServiceAccount {
	return d.object
}

func (d *ServiceAccountManager) Update(current client.Object) (UpdateType, error) {
	serviceAccount, ok := current.(*core.ServiceAccount)
	if !ok {
		return NoAction, errors.New("Incompatible type passed, only ServiceAccount objects are allowed")
	}

	updateType := NoAction

	if equality.Semantic.DeepEqual(d.object.Secrets, serviceAccount.Secrets) {
		updateType.Upgrade(NeedsUpdate)
		serviceAccount.Secrets = d.object.Secrets
	}

	if equality.Semantic.DeepEqual(d.object.ImagePullSecrets, serviceAccount.ImagePullSecrets) {
		updateType.Upgrade(NeedsUpdate)
		serviceAccount.ImagePullSecrets = d.object.ImagePullSecrets
	}

	if equality.Semantic.DeepEqual(d.object.AutomountServiceAccountToken, serviceAccount.AutomountServiceAccountToken) {
		updateType.Upgrade(NeedsUpdate)
		serviceAccount.AutomountServiceAccountToken = d.object.AutomountServiceAccountToken
	}

	if UpdateObjectMeta(d.object.ObjectMeta, serviceAccount.ObjectMeta) {
		updateType.Upgrade(NeedsUpdate)
	}

	return NoAction, nil
}

func NewServiceAccountManager(serviceAccount *core.ServiceAccount) *ServiceAccountManager {
	return &ServiceAccountManager{object: serviceAccount}
}

type RoleManager struct {
	object *rbac.Role
	identifier
}

func (d *RoleManager) New() client.Object {
	return &rbac.Role{}
}

func (d *RoleManager) Get() client.Object {
	return d.object
}

func (d *RoleManager) Role() *rbac.Role {
	return d.object
}

func (d *RoleManager) Update(current client.Object) (UpdateType, error) {
	role, ok := current.(*rbac.Role)
	if !ok {
		return NoAction, errors.New("Incompatible type passed, only Role objects are allowed")
	}

	updateType := NoAction

	if equality.Semantic.DeepEqual(d.object.Rules, role.Rules) {
		updateType.Upgrade(NeedsUpdate)
		role.Rules = d.object.Rules
	}

	if UpdateObjectMeta(d.object.ObjectMeta, role.ObjectMeta) {
		updateType.Upgrade(NeedsUpdate)
	}

	return NoAction, nil
}

func NewRoleManager(role *rbac.Role) *RoleManager {
	return &RoleManager{object: role}
}

type RoleBindingManager struct {
	object *rbac.RoleBinding
	identifier
}

func (d *RoleBindingManager) New() client.Object {
	return &rbac.RoleBinding{}
}

func (d *RoleBindingManager) Get() client.Object {
	return d.object
}

func (d *RoleBindingManager) Role() *rbac.RoleBinding {
	return d.object
}

func (d *RoleBindingManager) Update(current client.Object) (UpdateType, error) {
	binding, ok := current.(*rbac.RoleBinding)
	if !ok {
		return NoAction, errors.New("Incompatible type passed, only RoleBinding objects are allowed")
	}

	updateType := NoAction

	if equality.Semantic.DeepEqual(d.object.Subjects, binding.Subjects) {
		updateType.Upgrade(NeedsUpdate)
		binding.Subjects = d.object.Subjects
	}

	if equality.Semantic.DeepEqual(d.object.RoleRef, binding.RoleRef) {
		updateType.Upgrade(NeedsUpdate)
		binding.RoleRef = d.object.RoleRef
	}

	if UpdateObjectMeta(d.object.ObjectMeta, binding.ObjectMeta) {
		updateType.Upgrade(NeedsUpdate)
	}

	return NoAction, nil
}

func NewRoleBindingManager(binding *rbac.RoleBinding) *RoleBindingManager {
	return &RoleBindingManager{object: binding}
}
