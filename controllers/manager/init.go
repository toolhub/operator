package manager

import (
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// UpdateType demonstrates type of update needed for an object.
type UpdateType uint8

const (
	// NoAction means no action is necessary.
	NoAction UpdateType = iota

	// NeedsUpdate means an update to the object is necessary.
	NeedsUpdate

	// NeedsReplace means the object must be first deleted and then added again.
	// This is reserved for rare occasions where immutable fields have to be changed.
	NeedsReplace
)

// Upgrade increases the update action severity if needed.
func (u *UpdateType) Upgrade(other UpdateType) {
	if other > *u {
		*u = other
	}
}

// ObjectManager describes the ability to manage a kubernetes object.
type ObjectManager interface {
	// Get returns the desired object.
	Get() client.Object

	// New must return an empty object of the same type.
	New() client.Object

	// ID returns a unique id for this resource.
	ID() string

	// Update makes adjustments to the current object, if needed, in order to reflect the new desired state.
	// Either way, the inner object will get replaced by the given object.
	Update(current client.Object) (UpdateType, error)
}

// ObjectManagerList holds a collection of managed objects.
type ObjectManagerList []ObjectManager

// ManagerWithID returns managed object with the given id, if found.
func (o ObjectManagerList) ManagerWithID(id string) ObjectManager {
	for _, manager := range o {
		if manager.ID() == id {
			return manager
		}
	}
	return nil
}
