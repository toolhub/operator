package manager

const (
	// LabelToolHub indicates the name of the owning Repository
	LabelToolHub = "toolhub.io/repo"

	// LabelAgent indicates the name of the owning Agent
	LabelAgent = "toolhub.io/agent"

	// LabelComponent indicates the component. e.g. dispatcher.
	LabelComponent = "toolhub.io/component"
)

const (
	// AnnotationDescription holds the description of the function in the metadata.
	AnnotationDescription = "toolhub.io/description"

	AnnotiationOwnerID   = "toolhub.io/owner-id"
	AnnotiationOwnerName = "toolhub.io/owner-name"

	AnnotationVersion   = "toolhub.io/version"
	AnnotationSourceURL = "toolhub.io/source-url"
	AnnotationTags      = "toolhub.io/tags"

	AnnotationName  = "toolhub.io/name"
	AnnotationGroup = "toolhub.io/group"

	AnnotationRepo = "toolhub.io/repo"
)
