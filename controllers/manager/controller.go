package manager

import (
	"context"
	"fmt"

	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

type ControllerManager struct {
	Client client.Client
	Scheme *runtime.Scheme
}

func (c *ControllerManager) InstallObjects(ctx context.Context, list ObjectManagerList, owner client.Object) error {
	for _, object := range list {
		existingObject := object.New()
		desiredObject := object.Get()
		name := client.ObjectKey{Name: desiredObject.GetName(), Namespace: desiredObject.GetNamespace()}

		if err := ctrl.SetControllerReference(owner, desiredObject, c.Scheme); err != nil {
			return fmt.Errorf("could not set owner reference for %v: %s", name, err)
		}

		err := c.Client.Get(ctx, name, existingObject)
		if apierrors.IsNotFound(err) {
			// Create it
			if err := c.Client.Create(ctx, desiredObject); err != nil {
				return fmt.Errorf("could not create %v: %s", name, err)
			}
			continue
		}

		if err != nil {
			return fmt.Errorf("failed to retrieve %v: %s", name, err)
		}

		updateType, err := object.Update(existingObject)
		if err != nil {
			return fmt.Errorf("failed to compare %v: %s", name, err)
		}

		switch updateType {
		case NeedsUpdate:
			if err := c.Client.Update(ctx, object.Get()); err != nil {
				return fmt.Errorf("failed to update %v: %s", name, err)
			}
		case NeedsReplace:
			if err := c.Client.Delete(ctx, existingObject); err != nil {
				return fmt.Errorf("failed to replace %v: %s", name, err)
			}
			if err := c.Client.Create(ctx, object.Get()); err != nil {
				return fmt.Errorf("failed to replace %v: %s", name, err)
			}
		}
	}
	return nil
}

func (c *ControllerManager) DeleteObjects(ctx context.Context, objects []client.Object) error {
	for _, object := range objects {
		name := client.ObjectKey{Name: object.GetName(), Namespace: object.GetNamespace()}
		err := c.Client.Get(ctx, name, object)
		if err != nil {
			if apierrors.IsNotFound(err) {
				continue
			}
			return fmt.Errorf("failed to retrieve %v: %s", name, err)
		}
		if err := c.Client.Delete(ctx, object); err != nil {
			return fmt.Errorf("failed to delete %v: %s", name, err)
		}
	}
	return nil
}
