package repo

import (
	"fmt"

	v1alpha1 "gitlab.com/toolhub/operator/api/v1alpha1"
	"gitlab.com/toolhub/operator/controllers/manager"
	apps "k8s.io/api/apps/v1"
	core "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
)

const (
	serverContainerPort = 4000
)

func DispatcherLabels(repo *v1alpha1.Repository) map[string]string {
	return map[string]string{
		manager.LabelToolHub:   repo.Name,
		manager.LabelComponent: "dispatcher",
	}
}

func DispatcherServiceName(repo *v1alpha1.Repository) string {
	return fmt.Sprintf("%s-dispatcher", repo.Name)
}

func DispatcherDeploymentName(repo *v1alpha1.Repository) string {
	return fmt.Sprintf("%s-dispatcher", repo.Name)
}

func DispatcherService(repo *v1alpha1.Repository) *manager.ServiceManager {
	labels := DispatcherLabels(repo)

	service := &core.Service{
		ObjectMeta: meta.ObjectMeta{
			Name:      DispatcherServiceName(repo),
			Namespace: repo.Namespace,
			Labels:    labels,
		},
		Spec: core.ServiceSpec{
			Ports: []core.ServicePort{
				{
					Name:       "http",
					Port:       80,
					TargetPort: intstr.FromInt(serverContainerPort),
				},
			},
			Selector: labels,
		},
	}

	return manager.NewServiceManager(service)
}

func DispatcherResources(repo *v1alpha1.Repository) core.ResourceRequirements {
	return core.ResourceRequirements{
		Limits: core.ResourceList{
			core.ResourceCPU:    resource.MustParse("1"),
			core.ResourceMemory: resource.MustParse("500Mi"),
		},
		Requests: core.ResourceList{
			core.ResourceCPU:    resource.MustParse("100m"),
			core.ResourceMemory: resource.MustParse("200Mi"),
		},
	}
}

func DispatcherDeployment(repo *v1alpha1.Repository) *manager.DeploymentManager {
	labels := DispatcherLabels(repo)

	enableServiceLinks := false

	podTemplate := core.PodTemplateSpec{
		ObjectMeta: meta.ObjectMeta{
			Labels: labels,
		},
		Spec: core.PodSpec{
			Containers: []core.Container{
				{
					Name:            "server",
					Image:           repo.Spec.Dispatcher.Image,
					ImagePullPolicy: core.PullAlways,
					Ports: []core.ContainerPort{
						{
							Name:          "http",
							ContainerPort: serverContainerPort,
						},
					},
					VolumeMounts: []core.VolumeMount{
						{
							Name:      "dispatcher-config",
							MountPath: "/etc/toolstudio/dispatcher/",
						},
					},
					Command: []string{
						"/opt/toolstudio/dispatcher",
						"--config", "/etc/toolstudio/dispatcher/functions.json",
						"--endpoints-config", "/etc/toolstudio/dispatcher/access_endpoints.json",
						"--addr", fmt.Sprintf(":%d", serverContainerPort),
					},
					Resources: DispatcherResources(repo),
				},
			},
			EnableServiceLinks: &enableServiceLinks,
			Volumes:            []core.Volume{ConfigVolumeSource(repo, "dispatcher-config")},
		},
	}

	var revisionHistoryLimit int32 = 1

	deployment := &apps.Deployment{
		ObjectMeta: meta.ObjectMeta{
			Name:      DispatcherDeploymentName(repo),
			Namespace: repo.Namespace,
		},
		Spec: apps.DeploymentSpec{
			Replicas: &repo.Spec.Dispatcher.MinimumReplicas,
			Selector: &meta.LabelSelector{
				MatchLabels: labels,
			},
			Template:             podTemplate,
			RevisionHistoryLimit: &revisionHistoryLimit,
		},
	}

	return manager.NewDeploymentManager(deployment)
}
