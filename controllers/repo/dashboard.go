package repo

import (
	"fmt"

	v1alpha1 "gitlab.com/toolhub/operator/api/v1alpha1"
	"gitlab.com/toolhub/operator/controllers/manager"
	apps "k8s.io/api/apps/v1"
	core "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
)

const (
	dashboardContainerPort = 4000
)

func DashboardLabels(repo *v1alpha1.Repository) map[string]string {
	return map[string]string{
		manager.LabelToolHub:   repo.Name,
		manager.LabelComponent: "dashboard",
	}
}

func DashboardServiceName(repo *v1alpha1.Repository) string {
	return fmt.Sprintf("%s-dashboard", repo.Name)
}

func DashboardDeploymentName(repo *v1alpha1.Repository) string {
	return fmt.Sprintf("%s-dashboard", repo.Name)
}

func DashboardService(repo *v1alpha1.Repository) *manager.ServiceManager {
	labels := DashboardLabels(repo)

	service := &core.Service{
		ObjectMeta: meta.ObjectMeta{
			Name:      DashboardServiceName(repo),
			Namespace: repo.Namespace,
			Labels:    labels,
		},
		Spec: core.ServiceSpec{
			Ports: []core.ServicePort{
				{
					Name:       "http",
					Port:       80,
					TargetPort: intstr.FromInt(dashboardContainerPort),
				},
			},
			Selector: labels,
		},
	}

	return manager.NewServiceManager(service)
}

func DashboardResources(repo *v1alpha1.Repository) core.ResourceRequirements {
	return core.ResourceRequirements{
		Limits: core.ResourceList{
			core.ResourceCPU:    resource.MustParse("1"),
			core.ResourceMemory: resource.MustParse("500Mi"),
		},
		Requests: core.ResourceList{
			core.ResourceCPU:    resource.MustParse("100m"),
			core.ResourceMemory: resource.MustParse("200Mi"),
		},
	}
}

func DashboardDeployment(repo *v1alpha1.Repository) *manager.DeploymentManager {
	labels := DashboardLabels(repo)

	podTemplate := core.PodTemplateSpec{
		ObjectMeta: meta.ObjectMeta{
			Labels: labels,
		},
		Spec: core.PodSpec{
			Containers: []core.Container{
				{
					Name:            "nginx",
					Image:           repo.Spec.Dashboard.Image,
					ImagePullPolicy: core.PullAlways,
					Ports: []core.ContainerPort{
						{
							Name:          "http",
							ContainerPort: dashboardContainerPort,
						},
					},
					Resources: DispatcherResources(repo),
				},
			},
		},
	}

	var revisionHistoryLimit int32 = 1

	deployment := &apps.Deployment{
		ObjectMeta: meta.ObjectMeta{
			Name:      DashboardDeploymentName(repo),
			Namespace: repo.Namespace,
		},
		Spec: apps.DeploymentSpec{
			Replicas: &repo.Spec.Dashboard.MinimumReplicas,
			Selector: &meta.LabelSelector{
				MatchLabels: labels,
			},
			Template:             podTemplate,
			RevisionHistoryLimit: &revisionHistoryLimit,
		},
	}

	return manager.NewDeploymentManager(deployment)
}
