package repo

import (
	"fmt"

	v1alpha1 "gitlab.com/toolhub/operator/api/v1alpha1"
	"gitlab.com/toolhub/operator/controllers/manager"
	apps "k8s.io/api/apps/v1"
	core "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
)

func CDSLabels(repo *v1alpha1.Repository) map[string]string {
	return map[string]string{
		manager.LabelToolHub:   repo.Name,
		manager.LabelComponent: "cds",
	}
}

func CDSServiceName(repo *v1alpha1.Repository) string {
	return fmt.Sprintf("%s-cds", repo.Name)
}

func CDSDeploymentName(repo *v1alpha1.Repository) string {
	return fmt.Sprintf("%s-cds", repo.Name)
}

// CDSService returns object manager for CDS service.
func CDSService(repo *v1alpha1.Repository) *manager.ServiceManager {
	labels := CDSLabels(repo)

	service := &core.Service{
		ObjectMeta: meta.ObjectMeta{
			Name:      CDSServiceName(repo),
			Namespace: repo.Namespace,
			Labels:    labels,
		},
		Spec: core.ServiceSpec{
			Ports: []core.ServicePort{
				{
					Name:       "http",
					Port:       80,
					TargetPort: intstr.FromInt(serverContainerPort),
				},
				{
					Name:       "cds",
					Port:       5000,
					TargetPort: intstr.FromInt(5000),
				},
			},
			Selector: labels,
		},
	}

	return manager.NewServiceManager(service)
}

func CDSResources(repo *v1alpha1.Repository) core.ResourceRequirements {
	return core.ResourceRequirements{
		Limits: core.ResourceList{
			core.ResourceCPU:    resource.MustParse("1"),
			core.ResourceMemory: resource.MustParse("500Mi"),
		},
		Requests: core.ResourceList{
			core.ResourceCPU:    resource.MustParse("100m"),
			core.ResourceMemory: resource.MustParse("200Mi"),
		},
	}
}

func CDSDeployment(repo *v1alpha1.Repository) *manager.DeploymentManager {
	labels := CDSLabels(repo)

	enableServiceLinks := false

	podTemplate := core.PodTemplateSpec{
		ObjectMeta: meta.ObjectMeta{
			Labels: labels,
		},
		Spec: core.PodSpec{
			Containers: []core.Container{
				{
					Name:            "server",
					Image:           repo.Spec.ContentDelivery.ServerImage,
					ImagePullPolicy: core.PullAlways,
					Ports: []core.ContainerPort{
						{
							Name:          "http",
							ContainerPort: serverContainerPort,
						},
					},
					VolumeMounts: []core.VolumeMount{
						{Name: "data", MountPath: "/var/www/functions"},
						{Name: "cds-config", MountPath: "/etc/toolstudio/cds"},
					},
					Command: []string{
						"/opt/toolstudio/cds-nginx",
						"--endpoints-config",
						"/etc/toolstudio/cds/access_endpoints.json",
					},
					Resources: CDSResources(repo),
				},
				{
					Name:            "probe",
					Image:           repo.Spec.ContentDelivery.ProbeImage,
					ImagePullPolicy: core.PullAlways,
					VolumeMounts: []core.VolumeMount{
						{Name: "cds-config", MountPath: "/etc/toolstudio/cds"},
						{Name: "data", MountPath: "/var/www/functions"},
					},
					Ports: []core.ContainerPort{
						{
							Name:          "cds",
							ContainerPort: 5000,
						},
					},
					Command: []string{
						"/opt/toolstudio/cds",
						"--config",
						"/etc/toolstudio/cds/functions.json",
						"--dir",
						"/var/www/functions",
					},
					Resources: CDSResources(repo),
				},
			},
			EnableServiceLinks: &enableServiceLinks,
			Volumes: []core.Volume{
				ConfigVolumeSource(repo, "cds-config"),
				{Name: "data"},
			},
		},
	}

	var revisionHistoryLimit int32 = 1

	deployment := &apps.Deployment{
		ObjectMeta: meta.ObjectMeta{
			Name:      CDSDeploymentName(repo),
			Namespace: repo.Namespace,
		},
		Spec: apps.DeploymentSpec{
			Replicas: &repo.Spec.ContentDelivery.MinimumReplicas,
			Selector: &meta.LabelSelector{
				MatchLabels: labels,
			},
			Template:             podTemplate,
			RevisionHistoryLimit: &revisionHistoryLimit,
		},
	}

	return manager.NewDeploymentManager(deployment)
}
