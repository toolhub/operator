package repo

import (
	"fmt"

	v1alpha1 "gitlab.com/toolhub/operator/api/v1alpha1"
	core "k8s.io/api/core/v1"
)

func ConfigName(repo *v1alpha1.Repository) string {
	return fmt.Sprintf("%s-functions", repo.Name)
}

func ConfigVolumeSource(repo *v1alpha1.Repository, name string) core.Volume {
	return core.Volume{
		Name: name,
		VolumeSource: core.VolumeSource{
			ConfigMap: &core.ConfigMapVolumeSource{
				LocalObjectReference: core.LocalObjectReference{Name: ConfigName(repo)},
				Items: []core.KeyToPath{
					{
						Key:  "functions.json",
						Path: "functions.json",
					},
					{
						Key:  "access_endpoints.json",
						Path: "access_endpoints.json",
					},
				},
			},
		},
	}
}
