package repo

import (
	"fmt"

	v1alpha1 "gitlab.com/toolhub/operator/api/v1alpha1"
	agentobjs "gitlab.com/toolhub/operator/controllers/agent"
	"gitlab.com/toolhub/operator/controllers/manager"
	apps "k8s.io/api/apps/v1"
	core "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/apimachinery/pkg/util/uuid"
)

const (
	apiContainerPort        = 9000
	apiGatewayContainerPort = 9001
)

func APILabels(repo *v1alpha1.Repository) map[string]string {
	return map[string]string{
		manager.LabelToolHub:   repo.Name,
		manager.LabelComponent: "api",
	}
}

func APIServiceGRPCName(repo *v1alpha1.Repository) string {
	return fmt.Sprintf("%s-api", repo.Name)
}

func APIServiceHTTPName(repo *v1alpha1.Repository) string {
	return fmt.Sprintf("%s-api-grpc-web", repo.Name)
}

func APIDeploymentName(repo *v1alpha1.Repository) string {
	return fmt.Sprintf("%s-api", repo.Name)
}

func APISecretName(repo *v1alpha1.Repository) string {
	return fmt.Sprintf("%s-api", repo.Name)
}

// APIServiceGRPC returns object manager for gRPC API.
func APIServiceGRPC(repo *v1alpha1.Repository) *manager.ServiceManager {
	labels := APILabels(repo)

	service := &core.Service{
		ObjectMeta: meta.ObjectMeta{
			Name:      APIServiceGRPCName(repo),
			Namespace: repo.Namespace,
			Labels:    labels,
		},
		Spec: core.ServiceSpec{
			Ports: []core.ServicePort{
				{
					Name:       "grpc",
					Port:       80,
					TargetPort: intstr.FromInt(apiContainerPort),
				},
			},
			Selector: labels,
		},
	}

	return manager.NewServiceManager(service)
}

// APIServiceHTTP returns object manager for gRPC API web gateway.
func APIServiceHTTP(repo *v1alpha1.Repository) *manager.ServiceManager {
	labels := APILabels(repo)

	service := &core.Service{
		ObjectMeta: meta.ObjectMeta{
			Name:      APIServiceHTTPName(repo),
			Namespace: repo.Namespace,
			Labels:    labels,
		},
		Spec: core.ServiceSpec{
			Ports: []core.ServicePort{
				{
					Name:       "http",
					Port:       80,
					TargetPort: intstr.FromInt(apiGatewayContainerPort),
				},
			},
			Selector: labels,
		},
	}

	return manager.NewServiceManager(service)
}

func APIResources(repo *v1alpha1.Repository) core.ResourceRequirements {
	return core.ResourceRequirements{
		Limits: core.ResourceList{
			core.ResourceCPU:    resource.MustParse("1"),
			core.ResourceMemory: resource.MustParse("500Mi"),
		},
		Requests: core.ResourceList{
			core.ResourceCPU:    resource.MustParse("100m"),
			core.ResourceMemory: resource.MustParse("200Mi"),
		},
	}
}

func APISecret(repo *v1alpha1.Repository) *manager.SecretManager {
	secret := &core.Secret{
		ObjectMeta: meta.ObjectMeta{
			Name:      APISecretName(repo),
			Namespace: repo.Namespace,
			Labels:    APILabels(repo),
		},
		StringData: map[string]string{
			"primary_token": string(uuid.NewUUID()),
		},
	}
	return manager.NewSecretManager(secret, "primary_token")
}

func APIDeployment(repo *v1alpha1.Repository) *manager.DeploymentManager {
	labels := APILabels(repo)

	enableServiceLinks := false

	commands := []string{
		"/opt/toolstudio/api",
		"--endpoints-config", "/etc/toolstudio/api/access_endpoints.json",
		"--config", "/etc/toolstudio/api/functions.json",
		"--address", fmt.Sprintf(":%d", apiContainerPort),
		"--grpc-web-address", fmt.Sprintf(":%d", apiGatewayContainerPort),
	}

	if repo.Spec.ContentDelivery != nil {
		commands = append(commands, "--cds", fmt.Sprintf("%s-cds:5000", repo.Name))
	}

	volumes := []core.Volume{ConfigVolumeSource(repo, "api-config")}
	env := []core.EnvVar{}

	if repo.Spec.API.EnableModification {
		if agent := repo.Spec.API.Agent; agent != nil {
			commands = append(commands, "--delegate-api", fmt.Sprintf("%s-agent:8000", agent.Name))
			env = append(env, core.EnvVar{
				Name:  "TOOLSTUDIO_DELEGATE_TOKEN",
				Value: agentobjs.RepoToken(repo),
			})
			env = append(env, core.EnvVar{
				Name: "TOOLSTUDIO_PRIMARY_TOKEN",
				ValueFrom: &core.EnvVarSource{
					SecretKeyRef: &core.SecretKeySelector{
						LocalObjectReference: core.LocalObjectReference{
							Name: APISecretName(repo),
						},
						Key: "primary_token",
					},
				},
			})
		}
	}

	podTemplate := core.PodTemplateSpec{
		ObjectMeta: meta.ObjectMeta{
			Labels: labels,
		},
		Spec: core.PodSpec{
			EnableServiceLinks: &enableServiceLinks,
			Containers: []core.Container{
				{
					Name:            "api",
					Image:           repo.Spec.API.Image,
					ImagePullPolicy: core.PullAlways,
					Env:             env,
					VolumeMounts: []core.VolumeMount{
						{
							Name:      "api-config",
							MountPath: "/etc/toolstudio/api/",
						},
					},
					Ports: []core.ContainerPort{
						{
							Name:          "api",
							ContainerPort: apiContainerPort,
						},
						{
							Name:          "api-grpc-web",
							ContainerPort: apiGatewayContainerPort,
						},
					},
					Command:   commands,
					Resources: APIResources(repo),
				},
			},
			Volumes: volumes,
		},
	}

	var revisionHistoryLimit int32 = 1

	deployment := &apps.Deployment{
		ObjectMeta: meta.ObjectMeta{
			Name:      APIDeploymentName(repo),
			Namespace: repo.Namespace,
		},
		Spec: apps.DeploymentSpec{
			Replicas: &repo.Spec.API.MinimumReplicas,
			Selector: &meta.LabelSelector{
				MatchLabels: labels,
			},
			Template:             podTemplate,
			RevisionHistoryLimit: &revisionHistoryLimit,
		},
	}

	return manager.NewDeploymentManager(deployment)
}
