package repo

import (
	"encoding/json"
	"strings"

	"gitlab.com/toolhub/operator/api/v1alpha1"
	"gitlab.com/toolhub/operator/controllers/manager"
	"gitlab.com/toolhub/toolhub/pkg/api"
	"gitlab.com/toolhub/toolhub/pkg/function"
	core "k8s.io/api/core/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type functionConfig struct {
	Items []function.Descriptor `json:"items,omitempty"`
}

func processURL(url string) *api.RPCEndpoint {
	result := &api.RPCEndpoint{}
	if strings.HasPrefix(url, "/") {
		result.Grpc = "/"
		result.Gateway = url
	} else {
		index := strings.Index(url, "/")
		result.Grpc = url
		result.Gateway = url
		if index != -1 {
			result.Grpc = url[:index]
		}
	}
	return result
}

func accessEndpointsJSON(repo *v1alpha1.Repository) ([]byte, error) {
	// access endpoints.
	// first add all the additionalAccessEndpoints
	endpoints := map[string]*api.AccessEndpoint{}
	//for _, item := range repo.Spec.AdditionalAccessEndpoints {
	//  info := map[string]interface{}{
	//    "api":        item.API,
	//    "dispatcher": item.Dispatcher,
	//    "dashboard":  item.Dashboard,
	//    "insecure":   item.Insecure,
	//  }
	//  if repo.Spec.ContentDelivery != nil {
	//    info["cds"] = item.CDS
	//  }
	//  endpoints[item.Hostname] = info
	//}
	// then add entries from the ingress
	if repo.Spec.Ingress != nil {
		hostname := "*"
		if len(repo.Spec.Ingress.Location) > 0 {
			hostname = repo.Spec.Ingress.Location
		}
		info := &api.AccessEndpoint{
			Api:        processURL(repo.Spec.Ingress.Paths.API),
			Dispatcher: &api.RPCEndpoint{Http: repo.Spec.Ingress.Paths.Dispatcher},
			Dashboard:  repo.Spec.Ingress.Paths.Dashboard,
			Insecure:   len(repo.Spec.Ingress.TLS) == 0,
		}
		if repo.Spec.Ingress.SeparateIngressObjects.API != nil {
			info.Api = processURL(repo.Spec.Ingress.SeparateIngressObjects.API.Location)
		}
		if repo.Spec.ContentDelivery != nil {
			info.Cds = repo.Spec.Ingress.Paths.CDS
			if repo.Spec.Ingress.SeparateIngressObjects.CDS != nil {
				info.Cds = repo.Spec.Ingress.SeparateIngressObjects.CDS.Location
			} else {
				info.Cds = repo.Spec.Ingress.Paths.CDS
			}
		}
		if repo.Spec.Ingress.SeparateIngressObjects.Dispatcher != nil {
			info.Dispatcher = &api.RPCEndpoint{Http: repo.Spec.Ingress.SeparateIngressObjects.Dispatcher.Location}
		}
		if repo.Spec.Ingress.SeparateIngressObjects.Dashboard != nil {
			info.Dispatcher = &api.RPCEndpoint{Http: repo.Spec.Ingress.SeparateIngressObjects.Dashboard.Location}
		}
		endpoints[hostname] = info
	}

	return json.Marshal(map[string]interface{}{"endpoints": endpoints})
}

func RepositoryConfig(repo *v1alpha1.Repository, funcs []v1alpha1.Function) (*manager.ConfigMapManager, error) {
	items := make([]function.Descriptor, 0, len(funcs))

	for _, item := range funcs {
		name := item.Annotations[manager.AnnotationName]
		if len(name) == 0 {
			name = item.Name
		}
		group := item.Annotations[manager.AnnotationGroup]
		if len(group) == 0 {
			group = repo.Spec.DefaultGroup
		}
		descriptor := function.Descriptor{
			Name:  name,
			Group: group,
			Backend: function.BackendSpec{
				Endpoint: item.Status.Endpoint,
			},
		}
		status := function.StatusInstalling
		switch item.Status.Phase {
		case v1alpha1.PhaseError:
			status = function.StatusError
		case v1alpha1.PhaseRunning:
			status = function.StatusReady
		}
		descriptor.Status = &status
		if item.Spec.Frontend != nil {
			descriptor.Frontend = &function.FrontendSpec{
				Source: item.Spec.Frontend.Source,
			}
		}
		ownerID := item.Annotations[manager.AnnotiationOwnerID]
		ownerName := item.Annotations[manager.AnnotiationOwnerName]
		if len(ownerID) > 0 || len(ownerName) > 0 {
			descriptor.Owner = &function.Account{ID: ownerID, Name: ownerName}
		}

		descriptor.Meta = &function.Metadata{
			Description: item.Annotations[manager.AnnotationDescription],
			SourceURL:   item.Annotations[manager.AnnotationSourceURL],
			Version:     item.Annotations[manager.AnnotationVersion],
		}

		if tags, ok := item.Annotations[manager.AnnotationTags]; ok {
			parts := strings.Split(strings.ReplaceAll(tags, " ", ""), ",")
			descriptor.Meta.Tags = parts
		}
		items = append(items, descriptor)
	}

	functionsData, err := json.Marshal(functionConfig{Items: items})
	if err != nil {
		return nil, err
	}

	accessEndpointsData, err := accessEndpointsJSON(repo)
	if err != nil {
		return nil, err
	}

	configMap := &core.ConfigMap{
		ObjectMeta: meta.ObjectMeta{
			Name:      ConfigName(repo),
			Namespace: repo.Namespace,
		},
		BinaryData: map[string][]byte{
			"functions.json":        functionsData,
			"access_endpoints.json": accessEndpointsData,
		},
	}

	return manager.NewConfigMapManager(configMap), nil
}
