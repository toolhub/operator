package agent

import (
	"encoding/json"
	"fmt"

	"gitlab.com/toolhub/operator/api/v1alpha1"
	"gitlab.com/toolhub/operator/controllers/manager"
	"gitlab.com/toolhub/operator/pkg/agent"
	apps "k8s.io/api/apps/v1"
	core "k8s.io/api/core/v1"
	rbac "k8s.io/api/rbac/v1"
	meta "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
)

const (
	serverPort   = 8000
	defaultImage = "registry.gitlab.com/toolhub/operator/agent"
)

func serviceAccountName(a *v1alpha1.Agent) string {
	return fmt.Sprintf("%s-agent", a.Name)
}

func roleName(a *v1alpha1.Agent) string {
	return fmt.Sprintf("%s-function-manager", a.Name)
}

func configMapName(a *v1alpha1.Agent) string {
	return fmt.Sprintf("%s-agent-config", a.Name)
}

// RepoToken returns the token for a given repository.
func RepoToken(repo *v1alpha1.Repository) string {
	return string(repo.UID)
}

func labels(a *v1alpha1.Agent) map[string]string {
	return map[string]string{
		manager.LabelAgent:     a.Name,
		manager.LabelComponent: "agent",
	}
}

// ReplicaSet creates an object manager for the replica set.
func ReplicaSet(a *v1alpha1.Agent) *manager.ReplicaSetManager {
	labels := labels(a)

	no := false

	image := defaultImage
	if a.Spec.Image != nil {
		image = *a.Spec.Image
	}

	commands := []string{
		"/opt/toolstudio/operator/agent",
		"--repos-config", "/etc/toolstudio/operator/repos.json",
		"--addr", fmt.Sprintf(":%d", serverPort),
	}

	replicaSet := &apps.ReplicaSet{
		ObjectMeta: meta.ObjectMeta{
			Name:      fmt.Sprintf("%s-agent", a.Name),
			Namespace: a.Namespace,
			Labels:    labels,
		},
		Spec: apps.ReplicaSetSpec{
			Template: core.PodTemplateSpec{
				ObjectMeta: meta.ObjectMeta{Labels: labels},
				Spec: core.PodSpec{
					ServiceAccountName: serviceAccountName(a),
					EnableServiceLinks: &no,
					Volumes: []core.Volume{
						{
							Name: "agent-config",
							VolumeSource: core.VolumeSource{
								ConfigMap: &core.ConfigMapVolumeSource{
									LocalObjectReference: core.LocalObjectReference{
										Name: configMapName(a),
									},
									Items: []core.KeyToPath{{Key: "repos.json", Path: "repos.json"}},
								},
							},
						},
					},
					Containers: []core.Container{
						{
							Name:            "server",
							ImagePullPolicy: core.PullAlways,
							Image:           image,
							Command:         commands,
							Ports: []core.ContainerPort{
								{Name: "grpc", ContainerPort: serverPort},
							},
							VolumeMounts: []core.VolumeMount{
								{Name: "agent-config", MountPath: "/etc/toolstudio/operator"},
							},
						},
					},
				},
			},
			Selector: &meta.LabelSelector{
				MatchLabels: labels,
			},
		},
	}

	return manager.NewReplicaSetManager(replicaSet)
}

// ConfigMap creates desired state of a config map that holds all repo configs.
func ConfigMap(a *v1alpha1.Agent, repos []v1alpha1.Repository) (*manager.ConfigMapManager, error) {
	items := agent.ServiceTargetStore{}

	for _, repo := range repos {
		items[RepoToken(&repo)] = agent.RepositoryReference{Name: repo.Name, Namespace: repo.Namespace}
	}

	repoConfig := agent.RepoConfig{Repos: items}
	reposData, err := json.Marshal(repoConfig)
	if err != nil {
		return nil, err
	}

	configMap := &core.ConfigMap{
		ObjectMeta: meta.ObjectMeta{
			Name:      configMapName(a),
			Namespace: a.Namespace,
			Labels:    labels(a),
		},
		BinaryData: map[string][]byte{
			"repos.json": reposData,
		},
	}

	return manager.NewConfigMapManager(configMap), nil
}

// ServiceAccount returns the service account for the agent.
func ServiceAccount(a *v1alpha1.Agent) *manager.ServiceAccountManager {
	yes := true
	serviceAccount := &core.ServiceAccount{
		ObjectMeta: meta.ObjectMeta{
			Name:      serviceAccountName(a),
			Namespace: a.Namespace,
			Labels:    labels(a),
		},
		AutomountServiceAccountToken: &yes,
	}

	return manager.NewServiceAccountManager(serviceAccount)
}

func Role(a *v1alpha1.Agent) *manager.RoleManager {
	role := &rbac.Role{
		ObjectMeta: meta.ObjectMeta{
			Name:      roleName(a),
			Namespace: a.Namespace,
			Labels:    labels(a),
		},
		Rules: []rbac.PolicyRule{
			{
				APIGroups: []string{"function.toolhub.io"},
				Resources: []string{"functions"},
				Verbs:     []string{rbac.VerbAll},
			},
		},
	}

	return manager.NewRoleManager(role)
}

func RoleBinding(a *v1alpha1.Agent) *manager.RoleBindingManager {
	binding := &rbac.RoleBinding{
		ObjectMeta: meta.ObjectMeta{
			Name:      fmt.Sprintf("%s-agent", a.Name),
			Namespace: a.Namespace,
			Labels:    labels(a),
		},
		RoleRef: rbac.RoleRef{
			APIGroup: "rbac.authorization.k8s.io",
			Kind:     "Role",
			Name:     roleName(a),
		},
		Subjects: []rbac.Subject{
			{
				Kind:      "ServiceAccount",
				Name:      serviceAccountName(a),
				Namespace: a.Namespace,
			},
		},
	}

	return manager.NewRoleBindingManager(binding)
}

func Service(a *v1alpha1.Agent) *manager.ServiceManager {
	labels := labels(a)

	service := &core.Service{
		ObjectMeta: meta.ObjectMeta{
			Name:      fmt.Sprintf("%s-agent", a.Name),
			Namespace: a.Namespace,
			Labels:    labels,
		},
		Spec: core.ServiceSpec{
			Selector: labels,
			Ports: []core.ServicePort{
				{
					Name:       "grpc",
					Port:       8000,
					TargetPort: intstr.FromInt(serverPort),
				},
			},
		},
	}

	return manager.NewServiceManager(service)
}
