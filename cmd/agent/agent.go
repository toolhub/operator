package main

import (
	"context"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"strings"

	"gitlab.com/toolhub/operator/api/v1alpha1"
	"gitlab.com/toolhub/operator/controllers/manager"
	"gitlab.com/toolhub/operator/pkg/agent"
	"gitlab.com/toolhub/toolhub/pkg/api"
	"gitlab.com/toolhub/toolhub/pkg/watcher"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
)

// KubernetesDelegateServer is an in-cluster function service server.
type KubernetesDelegateServer struct {
	store  agent.ServiceTargetStore
	client client.Client

	api.UnimplementedFunctionServiceServer
}

func updateFunction(req *api.CreateFunctionRequest, function *v1alpha1.Function, ref agent.RepositoryReference) error {
	function.ObjectMeta.Name = req.Name
	function.ObjectMeta.Namespace = ref.Namespace
	if function.Annotations == nil {
		function.Annotations = map[string]string{}
	}
	if function.Labels == nil {
		function.Labels = map[string]string{}
	}
	function.Annotations[manager.AnnotationRepo] = ref.Name // make it exclusive to the target repository.
	if len(req.Group) > 0 {
		function.Annotations[manager.AnnotationGroup] = req.Group
		function.Annotations[manager.AnnotationName] = req.Name
		function.ObjectMeta.Name = fmt.Sprintf("%s-%s", req.Group, req.Name)
	}
	if backend := req.Backend; backend != nil {
		function.Spec.Backend.Image = backend.Image
		function.Spec.Backend.Type = backend.Type
		function.Spec.Backend.Port = backend.Port
	} else {
		return errors.New("missing backend spec")
	}
	if frontend := req.Frontend; frontend != nil {
		function.Spec.Frontend = &v1alpha1.FrontendSpec{
			Source: frontend.Source,
		}
	}
	if owner := req.Owner; owner != nil {
		function.Annotations[manager.AnnotiationOwnerID] = owner.Id
		function.Annotations[manager.AnnotiationOwnerName] = owner.Name
	}
	if meta := req.Metadata; meta != nil {
		if len(meta.Tags) > 0 {
			function.Annotations[manager.AnnotationTags] = strings.Join(meta.Tags, ",")
		}
		if len(meta.Description) > 0 {
			function.Annotations[manager.AnnotationDescription] = meta.Description
		}
		if len(meta.SourceUrl) > 0 {
			function.Annotations[manager.AnnotationSourceURL] = meta.SourceUrl
		}
		if len(meta.Version) > 0 {
			function.Annotations[manager.AnnotationVersion] = meta.Version
		}
	}
	function.Spec.MinimumReplicas = 1
	function.Spec.MaximumReplicas = 1
	return nil
}

// CreateFunction creates a function kubernetes object.
func (k *KubernetesDelegateServer) CreateFunction(ctx context.Context, req *api.CreateFunctionRequest) (*api.CreateFunctionResponse, error) {
	m, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, grpc.Errorf(codes.Unauthenticated, "authorization token is required")
	}
	tokens := m.Get("authorization")
	if len(tokens) != 1 {
		return nil, grpc.Errorf(codes.Unauthenticated, "exactly one authorization token must be provided")
	}
	target, ok := k.store[tokens[0]]
	if !ok {
		return nil, grpc.Errorf(codes.PermissionDenied, "permission denied to create Function objects in this cluster")
	}

	objectRef := client.ObjectKey{Name: req.Name, Namespace: target.Namespace}
	existingObject := &v1alpha1.Function{}
	if err := k.client.Get(context.Background(), objectRef, existingObject); err != nil {
		if err := updateFunction(req, existingObject, target); err != nil {
			return nil, grpc.Errorf(codes.InvalidArgument, "%s", err)
		}
		if err := k.client.Create(context.Background(), existingObject); err != nil {
			return nil, grpc.Errorf(codes.Internal, "failed: %s", err)
		}
		return &api.CreateFunctionResponse{}, nil
	}
	if err := updateFunction(req, existingObject, target); err != nil {
		return nil, grpc.Errorf(codes.InvalidArgument, "%s", err)
	}
	if err := k.client.Update(context.Background(), existingObject); err != nil {
		return nil, grpc.Errorf(codes.Internal, "failed to create kubernetes object Function %v: %s", objectRef, err)
	}
	return &api.CreateFunctionResponse{}, nil
}

func loadConfigFile(path string) (agent.ServiceTargetStore, error) {

	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	repoConfig := agent.RepoConfig{}
	if err := json.NewDecoder(file).Decode(&repoConfig); err != nil {
		return nil, nil
	}

	return repoConfig.Repos, nil
}

func main() {
	var repoConfigPath string
	var addr string

	flag.StringVar(&repoConfigPath, "repos-config", "", "the path to the repository configs.")
	flag.StringVar(&addr, "addr", ":8000", "the address to listen on.")
	flag.Parse()

	listener, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("failed to listen on '%s': %s", addr, err)
	}

	client, err := client.New(config.GetConfigOrDie(), client.Options{})
	if err != nil {
		log.Fatal(err)
	}
	v1alpha1.AddToScheme(client.Scheme())

	server := grpc.NewServer()

	service := &KubernetesDelegateServer{client: client}

	w := watcher.NewFileWatcher(repoConfigPath)
	store, err := loadConfigFile(repoConfigPath)
	if err != nil {
		log.Fatalf("failed to load repo config file: %s", err)
	}
	service.store = store
	go func() {
		for {
			select {
			case <-w.Events:
				if store, err := loadConfigFile(repoConfigPath); err != nil {
					log.Printf("failed to reload repo store: %s", err)
				} else {
					service.store = store
				}
			case err := <-w.Errors:
				log.Printf("file watcher error: %s", err)
			}
		}
	}()
	if err := w.Start(); err != nil {
		log.Fatalf("failed to watch repo config '%s': %s", repoConfigPath, err)
	}

	api.RegisterFunctionServiceServer(server, service)

	log.Fatalln(server.Serve(listener))
}
