package agent

// RepositoryReference holds details of a target repository.
type RepositoryReference struct {
	Name      string
	Namespace string
}

// ServiceTargetStore maps access tokens to target repository info.
type ServiceTargetStore map[string]RepositoryReference

// RepoConfig holds repository configs.
type RepoConfig struct {
	Repos ServiceTargetStore `json:"repos,omitempty"`
}
