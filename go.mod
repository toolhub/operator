module gitlab.com/toolhub/operator

go 1.16

require (
	gitlab.com/toolhub/toolhub v0.0.0-20210916152333-7edfb599149f
	google.golang.org/grpc v1.40.0
	k8s.io/api v0.21.3
	k8s.io/apimachinery v0.21.3
	k8s.io/client-go v0.21.3
	sigs.k8s.io/controller-runtime v0.9.6
)
